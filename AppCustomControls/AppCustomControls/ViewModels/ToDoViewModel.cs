﻿using AppCustomControls.Models;
using AppCustomControls.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace AppCustomControls.ViewModels
{
    public class ToDoViewModel : INotifyPropertyChanged
    {
        ApiService apiRest;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<ToDo> list { get; set; }

        public bool IsLoad { get; set; }

        public ToDoViewModel()
        {
            this.apiRest = new ApiService();
            Load();
        }

        private async void Load()
        {
            this.IsLoad = true;
            var aux = (List<ToDo>) await this.apiRest.Get<ToDo>("https://jsonplaceholder.typicode.com", "/todos");
            this.list = new ObservableCollection<ToDo>(aux);
            this.IsLoad = false;
        }
    }
}
