﻿using Xamarin.Forms;

namespace AppCustomControls.CustomControls
{
    public class CustomEntry : Entry
    {
        public CustomEntry()
        {
            if(Device.RuntimePlatform == Device.Android)
            {
                this.BackgroundColor = Color.LightSkyBlue;
                this.TextColor = Color.Blue;
                this.Placeholder = "Ingrese un texto";
                this.PlaceholderColor = Color.Black;
            }
            else if(Device.RuntimePlatform == Device.iOS)
            {
                this.BackgroundColor = Color.DeepSkyBlue;
                this.TextColor = Color.Yellow;
                this.Placeholder = "Inserte un texto";
                this.PlaceholderColor = Color.Silver;
            }
            else
            {
                this.BackgroundColor = Color.White;
                this.TextColor = Color.Black;
            }

            if(Device.Idiom == TargetIdiom.Phone)
            {
                this.HeightRequest = 50;
            }else if(Device.Idiom == TargetIdiom.Desktop)
            {
                this.HeightRequest = 60;
            }
            else
            {
                this.HeightRequest = 30;
            }
        }
    }
}