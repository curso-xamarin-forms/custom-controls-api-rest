﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppCustomControls.Services
{
    public class ApiService
    {
        public async Task<object> Get<T>(string urlBase, string prefijo)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.GetAsync(prefijo);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var list = JsonConvert.DeserializeObject<List<T>>(content);
                    return list;
                }
                return new List<T>();
            }
            catch
            {
                return new List<T>();
            }
            
        }
    }
}